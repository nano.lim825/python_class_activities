import copy


def bubble_sort(numbers_to_sort):
  pass


def insertion_sort(numbers_to_sort):
  pass


def selection_sort(numbers_to_sort):
  pass


numbers = [12, -2, 4, 8, 29, 45, 78, 36, -17, 2, 12, 12, 3, 3, -52]

# print the smallest number
# print the biggest number
# print all even numbers
# how often does the first number appear in the least?
# print the average
# print the sum of all the negative numbers

# sort the list using bubble sort
bubble_sort(numbers)

# sort the list using insertion sort
insertion_sort(numbers)

# sort the list using selection sort
selection_sort(numbers)
